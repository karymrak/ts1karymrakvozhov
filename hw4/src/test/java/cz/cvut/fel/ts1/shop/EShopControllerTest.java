package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.storage.NoItemInStorage;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Field;
import static org.junit.jupiter.api.Assertions.*;
public class EShopControllerTest {
    @Test
    void testStartEshop() throws IllegalAccessException, NoSuchFieldException {
        EShopController eShopController = new EShopController();
        EShopController.startEShop();

//        ACT
        Field archivef = EShopController.class.getDeclaredField("archive");
        Field storagef = EShopController.class.getDeclaredField("storage");
        Field cartsf = EShopController.class.getDeclaredField("carts");
        Field ordersf = EShopController.class.getDeclaredField("orders");
        archivef.setAccessible(true);
        storagef.setAccessible(true);
        cartsf.setAccessible(true);
        ordersf.setAccessible(true);

//        ASSERT
        assertNotNull(archivef.get(eShopController));
        assertNotNull(storagef.get(eShopController));
        assertNotNull(cartsf.get(eShopController));
        assertNotNull(ordersf.get(eShopController));
    }

    @Test
    public void PurchaseShoppingCartTest() throws NoItemInStorage {
        EShopController.startEShop();
        ShoppingCart cart = EShopController.newCart();

//        ACT
        int[] itemCount = {5, 7, 9};

        Item[] listOfItem = {
                new StandardItem(1, "Item 1", 10.0f, "Category1", 5),
                new StandardItem(2, "Item 2", 20.0f, "Category2",6),
                new StandardItem(3, "Item 3", 30.0f, "Category3",7)
        };

        for (int i = 0; i < listOfItem.length; i++) {
            EShopController.getStorage().insertItems(listOfItem[i], itemCount[i]);
        }

        cart.addItem(listOfItem[0]);
        cart.addItem(listOfItem[1]);
        cart.addItem(listOfItem[2]);

        cart.removeItem(listOfItem[0].getID());

        EShopController.purchaseShoppingCart(cart, "Aboba", "Dejvicka");

        //ASSERT
        assertEquals(1, EShopController.getArchive().getOrderArchive().size());
    }

    @Test
    void purchaseShoppingCart_EmptyCart_ThrowsIllegalStateException() {
        EShopController.startEShop();
        ShoppingCart emptyCart = EShopController.newCart();

        // ACT
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            EShopController.purchaseShoppingCart(emptyCart, "Tom", "dejvicka");
        });

        // ASSERT
        assertEquals("Cannot purchase an empty shopping cart", exception.getMessage());
    }
}