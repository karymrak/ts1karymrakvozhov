package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
public class StandardItemTest {
    @Test
    public void testConstructor() {
//        ACT
        StandardItem item = new StandardItem(1, "T-Shirt", 19.99f, "Clothing", 10);
//        ASSERT
        assertEquals(1, item.getID());
        assertEquals("T-Shirt", item.getName());
        assertEquals(19.99f, item.getPrice());
        assertEquals("Clothing", item.getCategory());
        assertEquals(10, item.getLoyaltyPoints());
    }

    @ParameterizedTest
    @MethodSource("equalityTestCases")
    public void testEquals(boolean expectedResult, StandardItem item1, StandardItem item2) {
//        ASSERT
        assertEquals(expectedResult, item1.equals(item2));
    }

    @Test
    public void testCopy() {
//        ACT
        StandardItem item1 = new StandardItem(1, "T-Shirt", 19.99f, "Clothing", 10);
        StandardItem item2 = item1.copy();
//        ASSERT
        assertNotSame(item1,item2);
        assertEquals(item1.getID(), item2.getID());
        assertEquals(item1.getName(), item2.getName());
        assertEquals(item1.getPrice(), item2.getPrice());
        assertEquals(item1.getCategory(), item2.getCategory());
        assertEquals(item1.getLoyaltyPoints(), item2.getLoyaltyPoints());
    }

    static Stream<Arguments> equalityTestCases() {
        StandardItem item1 = new StandardItem(1, "T-Shirt", 19.99f, "Clothing", 10);
        StandardItem item2 = new StandardItem(1, "T-Shirt", 19.99f, "Clothing", 10);
        StandardItem item3 = new StandardItem(2, "Shirt", 29.99f, "Clothing", 15);

        return Stream.of(
                Arguments.of(true, item1, item2),
                Arguments.of(false, item1, item3)
        );
    }
}
