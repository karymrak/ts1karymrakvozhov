package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrderTest {
    private ShoppingCart shoppingCart;
    private String customerName;
    private String customerAddress;

    @BeforeEach
    public void setUp() {
        shoppingCart = new ShoppingCart();
        customerName = "Tom Jerry";
        customerAddress = "123 Wall St";
    }

    @Test
    public void testConstructorWithStateAllArgumentsValid() {
        //ACT
        Order order = new Order(shoppingCart, customerName, customerAddress, 1);

        //ASSERT
        assertEquals(shoppingCart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(1, order.getState());
    }

    @Test
    public void testConstructorWithoutStateAllArgumentsValid() {
        //ACT
        Order order = new Order(shoppingCart, customerName, customerAddress);

        //ASSERT
        assertEquals(shoppingCart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
        public void nullTest() {
//        ASSERT
            assertThrows(NullPointerException.class, () -> {
                new Order(null,customerName,customerAddress,1);
            });
        }
}
