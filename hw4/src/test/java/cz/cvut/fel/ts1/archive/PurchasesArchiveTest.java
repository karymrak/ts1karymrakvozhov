package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PurchasesArchiveTest {
    @Test
    public void testGetHowManyTimesHasBeenItemSold() {
        //ACT
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        Item item1 = new Item(1, "Item 1", 15.0f, "ExampleCategory");
        ItemPurchaseArchiveEntry entry1 = new ItemPurchaseArchiveEntry(item1);
        entry1.increaseCountHowManyTimesHasBeenSold(6);
        Item item2 = new Item(2, "Item 2", 25.0f, "ExampleCategory");
        ItemPurchaseArchiveEntry entry2 = new ItemPurchaseArchiveEntry(item2);
        entry2.increaseCountHowManyTimesHasBeenSold(10);
        Item item3 = new Item(3, "Item 3", 55.0f, "ExampleCategory");
        itemPurchaseArchiveMock.put(1, entry1);
        itemPurchaseArchiveMock.put(2, entry2);

        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, null);

        //ASSERT
        assertEquals(7, archive.getHowManyTimesHasBeenItemSold(item1));
        assertEquals(11, archive.getHowManyTimesHasBeenItemSold(item2));
        assertEquals(0, archive.getHowManyTimesHasBeenItemSold(item3));
    }

    @Test
    public void testPutOrderToPurchasesArchive() {
//        ACT
        ShoppingCart shoppingCart = new ShoppingCart();
        Item item1 = new Item(1, "Item 1", 10.0f, "Category");
        Item item2 = new Item(2, "Item 2", 20.0f, "Category");
        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);

        ArrayList<Item> orderItems = new ArrayList<>();
        orderItems.add(item1);
        orderItems.add(item2);
        Order order = new Order(shoppingCart, "Test Customer", "Test Address");
        order.setItems(orderItems);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        itemPurchaseArchiveMock.put(1, new ItemPurchaseArchiveEntry(item1));
        itemPurchaseArchiveMock.put(2, new ItemPurchaseArchiveEntry(item2));

        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, new ArrayList<>());

        archive.putOrderToPurchasesArchive(order);

        //ASSERT
        assertEquals(1, archive.orderArchive.size());
        assertEquals(order, archive.orderArchive.get(0));
        assertEquals(2, archive.itemPurchaseArchive.get(1).getCountHowManyTimesHasBeenSold());
        assertEquals(2, archive.itemPurchaseArchive.get(2).getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void testPrintItemPurchaseStatistics() {
//        ACT
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        ItemPurchaseArchiveEntry entry1 = new ItemPurchaseArchiveEntry(new Item(1, "Item 1", 10.0f, "Category"));
        entry1.increaseCountHowManyTimesHasBeenSold(5);
        ItemPurchaseArchiveEntry entry2 = new ItemPurchaseArchiveEntry(new Item(2, "Item 2", 20.0f, "Category"));
        entry2.increaseCountHowManyTimesHasBeenSold(10);
        itemPurchaseArchiveMock.put(1, entry1);
        itemPurchaseArchiveMock.put(2, entry2);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        try {
            PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, null);
            archive.printItemPurchaseStatistics();

            String expectedOutput = "ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                    "ITEM  Item   ID 1   NAME Item 1   CATEGORY Category   HAS BEEN SOLD 6 TIMES" + System.lineSeparator() +
                    "ITEM  Item   ID 2   NAME Item 2   CATEGORY Category   HAS BEEN SOLD 11 TIMES" + System.lineSeparator();
            //ASSERT
            assertEquals(expectedOutput, outContent.toString());
        } finally {
            System.setOut(originalOut);
        }
    }

    @Test
    public void mockOrderArchive() {
//        ACT
        Order order = mock(Order.class);
        ArrayList<Item> orderItems = new ArrayList<>();
        Item item1 = new Item(1, "Item 1", 10.0f, "Category");
        Item item2 = new Item(2, "Item 2", 20.0f, "Category");
        orderItems.add(item1);
        orderItems.add(item2);
        when(order.getItems()).thenReturn(orderItems);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        itemPurchaseArchiveMock.put(1, new ItemPurchaseArchiveEntry(item1));
        itemPurchaseArchiveMock.put(2, new ItemPurchaseArchiveEntry(item2));

        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, mock(ArrayList.class));

        archive.putOrderToPurchasesArchive(order);

        verify(archive.orderArchive).add(order);

        //ASSERT
        assertEquals(2, archive.itemPurchaseArchive.get(1).getCountHowManyTimesHasBeenSold());
        assertEquals(2, archive.itemPurchaseArchive.get(2).getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void mockItemPurchaseArchiveEntry() {
//        ACT
        ItemPurchaseArchiveEntry mockEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockEntry.getCountHowManyTimesHasBeenSold()).thenReturn(0);

        Order mockOrder = mock(Order.class);
        ArrayList<Item> orderItems = new ArrayList<>();
        Item item1 = new Item(1, "Item 1", 10.0f, "Category");
        orderItems.add(item1);
        when(mockOrder.getItems()).thenReturn(orderItems);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveMock = new HashMap<>();
        itemPurchaseArchiveMock.put(1, mockEntry);
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchiveMock, new ArrayList<>());

        archive.putOrderToPurchasesArchive(mockOrder);

        verify(mockEntry).increaseCountHowManyTimesHasBeenSold(1);
    }

    @Test
    void testConstructor() {
//        ACT
        Item item = new StandardItem(1,"exampleName", 2.0f, "exampleCategory", 2);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);

//        ASSERT
        assertEquals(item, itemPurchaseArchiveEntry.getRefItem());
        assertEquals(1, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }

    @Test
    void testContructorNull() {
//        ACT
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(null);

//        ASSERT
        assertNull(itemPurchaseArchiveEntry.getRefItem());
        assertEquals(1, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }
}