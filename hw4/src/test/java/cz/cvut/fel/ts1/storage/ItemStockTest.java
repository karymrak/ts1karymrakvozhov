package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {
    @Test
    public void testItemStockConstructor() {
        //ACT
        Item item = new Item(1, "Example Item", 22.0f, "Example Category");
        ItemStock itemStock = new ItemStock(item);

        //ASSERT
        assertEquals(item, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "2",
            "11",
            "22"
    })
    public void testIncreaseItemCount(int increaseBy) {
        //ACT
        Item item = new Item(1, "Example Item", 22.0f, "Example Category");
        ItemStock itemStock = new ItemStock(item);

        itemStock.IncreaseItemCount(increaseBy);

        //ASSERT
        assertEquals(increaseBy, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "2",
            "11",
            "22"
    })
    public void testDecreaseItemCount(int decreaseBy) {
        //ACT
        Item item = new Item(1, "Example Item", 22.0f, "Example Category");
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(22);

        itemStock.decreaseItemCount(decreaseBy);

        //ASSERT
        assertEquals(22 - decreaseBy, itemStock.getCount());
    }
}
