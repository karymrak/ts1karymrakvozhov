import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.HomePage;
import pageObjects.LoginPage;
import pageObjects.SearchPage;
import java.time.Duration;

public class SearchTest {
    WebDriver driver;
    WebDriverWait wait;

    @BeforeEach
    public void setUp() {
        driver = new FirefoxDriver();
        driver.get("https://link.springer.com/");
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @ParameterizedTest
    @CsvFileSource(files = "savedArticles.txt")
    public void testSearchWithArguments(String expectedName, String expectedHref, String expectedDate) {

        HomePage springerHomePage = new HomePage(driver);

        springerHomePage.clickAcceptCookiesButtonIfExists();

        LoginPage springerLoginPage = springerHomePage.clickLoginButton();

        springerLoginPage.clickAcceptCookiesButtonIfExists();

        springerLoginPage.fillEmail("rmkarymshakov@gmail.com");

        springerLoginPage.submitEmail();

        springerLoginPage.fillPassword("1234567891011");

        springerLoginPage.submitPassword();

        driver.get("https://link.springer.com/search");
        SearchPage searchPage = new SearchPage(driver);
        springerHomePage.clickAcceptCookiesButtonIfExists();
        searchPage.fillSearchQuery(expectedName);
        searchPage.submitSearch();

        wait.until(driver -> searchPage.getFirstArticleName() != null);

        assertSearchWithArguments(expectedName, expectedHref, expectedDate, searchPage);
    }

    private void assertSearchWithArguments(String expectedName, String expectedHref, String expectedDate, SearchPage searchPage) {
        String actualName = searchPage.getFirstArticleName();
        String actualHref = searchPage.getFirstArticleDOI();
        String actualDate = searchPage.getFirstArticleDate();

        Assertions.assertEquals(expectedName, actualName);
        Assertions.assertEquals(expectedHref, actualHref);
        Assertions.assertEquals(expectedDate, actualDate);
    }
}
