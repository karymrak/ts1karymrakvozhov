package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchPage extends Page{
    public SearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/div[4]/div/search/form/div/div/div/div/input")
    private WebElement searchInput;

    @FindBy(xpath = "/html/body/div[4]/div/search/form/div/div/div/div/span/button")
    private WebElement searchSubmitButton;

    @FindBy(xpath = "/html/body/div[4]/div/div[2]/div/div[1]/div")
    private WebElement popupFilters;

    @FindBy (id = "content-type-article") // id cause xpath doesnt working
    private WebElement contentTypeArticleCheckbox;

    @FindBy(xpath = "/html/body/div[4]/div/div[2]/div/div[1]/div/div[2]/div/div[2]/div/details/div/div[2]/div[1]/div/input")
    private WebElement dateCustomCheckbox;

    @FindBy(id = "date-from") // id cause xpath doesnt working
    private WebElement dateFromInput;

    @FindBy(id = "date-to") // id cause xpath doesnt working
    private WebElement dateToInput;

    public void fillSearchQuery(String query) {
        wait.until(ExpectedConditions.elementToBeClickable(searchInput));
        searchInput.sendKeys(query);
    }

    public void chooseContentTypeArticle() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", contentTypeArticleCheckbox);
    }

    public String getFirstArticleName() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("h3.app-card-open__heading > a > span")).getText();
    }

    public String getFirstArticleDate() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("span[data-test='published']")).getText();
    }

    public String getFirstArticleDOI() {
        WebElement list = driver.findElement(By.cssSelector("ol.u-list-reset[data-test='darwin-search']"));
        WebElement firstItem = list.findElement(By.cssSelector("li[data-test='search-result-item']:first-of-type"));
        return firstItem.findElement(By.cssSelector("h3.app-card-open__heading > a")).getAttribute("href");
    }

    public void chooseCustomPublishedDate(String from, String to) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dateFromInput);
        wait.until(ExpectedConditions.elementToBeClickable(dateFromInput));
        dateFromInput.sendKeys(from);

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", dateToInput);
        wait.until(ExpectedConditions.elementToBeClickable(dateToInput));
        dateToInput.sendKeys(to);
    }

    public void submitSearch() {
        wait.until(ExpectedConditions.elementToBeClickable(searchSubmitButton));
        searchSubmitButton.click();
    }
}
