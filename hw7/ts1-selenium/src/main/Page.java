package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public abstract class Page {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public Page(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    }

    public void clickAcceptCookiesButtonIfExists() {
        try {
            By acceptCookiesButtonAddress = By.cssSelector("button[data-cc-action='accept']");
            WebElement acceptCookiesButton = wait.until(ExpectedConditions.elementToBeClickable(acceptCookiesButtonAddress));
            acceptCookiesButton.click();
        } catch (Exception ignored) {
        }
    }
}
