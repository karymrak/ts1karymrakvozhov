package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://link.springer.com/");
        HomePage homePage = new HomePage(driver);

        //log in
        homePage.clickAcceptCookiesButtonIfExists();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        LoginPage loginPage = homePage.clickLoginButton();
        loginPage.clickAcceptCookiesButtonIfExists();

        loginPage.fillEmail("rmkarymshakov@gmail.com");

        loginPage.submitEmail();

        loginPage.fillPassword("1234567891011");

        loginPage.submitPassword();

        // advanced searching
        driver.get("https://link.springer.com/advanced-search");
        AdvancedSearchPage springerAdvancedSearchPage = new AdvancedSearchPage(driver);

        new WebDriverWait(driver, Duration.ofSeconds(15));

        springerAdvancedSearchPage.fillAllWords("Page Object Model");
        new WebDriverWait(driver, Duration.ofSeconds(15));

        springerAdvancedSearchPage.fillLeastWords("Sellenium Testing");
        new WebDriverWait(driver, Duration.ofSeconds(15));

        springerAdvancedSearchPage.fillFacetStartYear("2024");
        new WebDriverWait(driver, Duration.ofSeconds(15));
        springerAdvancedSearchPage.fillFacetEndYear("2024");

        SearchPage springerSearchPage = springerAdvancedSearchPage.submitSearch();

        wait.until(ExpectedConditions.urlContains("search"));

        springerSearchPage.chooseCustomPublishedDate("2024", "2024");

        springerSearchPage.chooseContentTypeArticle();
        springerSearchPage.submitSearch();

        // save data
        Path filePath = Paths.get("savedArticles.txt");

        List<WebElement> items = driver.findElements(By.cssSelector("ol.u-list-reset[data-test='darwin-search'] li[data-test='search-result-item']"));

        for (int i = 0; i < 4; i++) {
            WebElement item = items.get(i);
            WebElement link = item.findElement(By.cssSelector("h3.app-card-open__heading > a"));
            String href = link.getAttribute("href");
            String articleName = link.findElement(By.tagName("span")).getText();
            String publishDate = item.findElement(By.cssSelector("span[data-test='published']")).getText();
            String line = String.format("\"%s\", \"%s\", \"%s\"\n", articleName, href, publishDate);

            try {
                Files.write(filePath, line.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        driver.quit();
    }
}