package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends Page {
    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/main/div/div/div/div[1]/form/div[1]/div/input")
    private WebElement emailInput;

    @FindBy(xpath = "/html/body/main/div/div/div/div[1]/form/div[3]/button")
    public WebElement emailSubmitButton;

    @FindBy(xpath = "/html/body/main/div/div/form/div[1]/span/input")
    private WebElement passwordInput;

    @FindBy(xpath = "/html/body/main/div/div/form/div[2]/button")
    private WebElement passwordSubmitButton;

    public void fillEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void submitEmail() {
        wait.until(ExpectedConditions.elementToBeClickable(emailSubmitButton));
        emailSubmitButton.click();
    }

    public void fillPassword(String password) {
        wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
        passwordInput.sendKeys(password);
    }

    public void submitPassword() {
        wait.until(ExpectedConditions.elementToBeClickable(passwordSubmitButton));
        passwordSubmitButton.click();
    }
}
