package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AdvancedSearchPage extends Page {
    public AdvancedSearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[1]/input")
    private WebElement allWordsInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[2]/input")
    private WebElement exactPhraseInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[3]/input")
    private WebElement leastWordsInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[4]/input")
    private WebElement withoutWordsInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[5]/input")
    private WebElement titleIsInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[6]/input")
    private WebElement authorIsInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[7]/div[1]/div/select")
    private WebElement dateFacetModeSelect;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[7]/div[1]/div/span[1]/input")
    private WebElement facetStartYearInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[7]/div[1]/div/span[3]/input")
    private WebElement facetEndYearInput;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[1]/div[7]/div[2]/div/input")
    private WebElement showAdvancedResultsCheckbox;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div[3]/div/div/form/div[2]/button")
    private WebElement submitButton;

    public void fillAllWords(String words) {
        wait.until(ExpectedConditions.elementToBeClickable(allWordsInput));
        allWordsInput.sendKeys(words);
    }

    public void fillLeastWords(String words) {
        wait.until(ExpectedConditions.elementToBeClickable(leastWordsInput));
        leastWordsInput.sendKeys(words);
    }

    public void selectDateFacetMode(String mode) {
        wait.until(ExpectedConditions.elementToBeClickable(dateFacetModeSelect));
        dateFacetModeSelect.sendKeys(mode);
    }

    public void fillFacetStartYear(String year) {
        wait.until(ExpectedConditions.elementToBeClickable(facetStartYearInput));
        facetStartYearInput.sendKeys(year);
    }
    public void fillFacetEndYear(String year) {
        wait.until(ExpectedConditions.elementToBeClickable(facetEndYearInput));
        facetEndYearInput.sendKeys(year);
    }

    public void checkShowLockedResults(boolean check) {
        wait.until(ExpectedConditions.elementToBeClickable(showAdvancedResultsCheckbox));
        if (check!= showAdvancedResultsCheckbox.isSelected()) {
            showAdvancedResultsCheckbox.click();
        }
    }

    public SearchPage submitSearch() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
        return new SearchPage(driver);
    }
}