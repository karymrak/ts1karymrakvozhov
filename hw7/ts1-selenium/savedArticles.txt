"Simultaneous Multi-View Object Recognition and Grasping in Open-Ended Domains", "https://link.springer.com/article/10.1007/s10846-024-02092-5", "16 April 2024"
"Improving Android app exploratory testing with UI test cases using code change analysis", "https://link.springer.com/article/10.1007/s11334-024-00555-4", "04 April 2024"
"A reinforcement learning-based approach to testing GUI of moblie applications", "https://link.springer.com/article/10.1007/s11280-024-01252-9", "22 February 2024"
"Histone 3 Trimethylation Patterns are Associated with Resilience or Stress Susceptibility in a Rat Model of Major Depression Disorder", "https://link.springer.com/article/10.1007/s12035-024-03912-3", "16 January 2024"
