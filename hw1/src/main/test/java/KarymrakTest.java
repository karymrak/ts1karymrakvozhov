import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KarymrakTest {
    @Test
    public void testFactorial5() {
        Karymrak karymrak = new Karymrak();
        assertEquals(120, karymrak.factorial(5));
    }

    @Test
    public void testFactorial0() {
        Karymrak karymrak = new Karymrak();
        assertEquals(120, karymrak.factorial(5));
    }

    @Test
    public void testFactorialNegativeNumber() {
        Karymrak karymrak = new Karymrak();
        assertThrows(IllegalArgumentException.class, () -> karymrak.factorial(-10));
    }
}