package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testCalculateAddition() {
        double result = calculator.calculateAddition(5, 3);
        Assertions.assertEquals(8, result, 0);
    }

    @Test
    public void testCalculateSubtraction() {
        double result = calculator.calculateSubtraction(5, 3);
        Assertions.assertEquals(2, result, 0);
    }

    @Test
    public void testCalculateMultiplication() {
        double result = calculator.calculateMultiplication(5, 3);
        Assertions.assertEquals(15, result, 0);
    }

    @Test
    public void testCalculateDivision() {
        double result = calculator.calculateDivision(12, 3);
        Assertions.assertEquals(4, result, 0);
    }

    @Test
    public void testCalculateDivisionByZero() {
        assertThrows(ArithmeticException.class, () -> calculator.calculateDivision(5, 0));
    }
}
