package cz.cvut.fel.ts1;

public class Calculator {
    public double calculateAddition(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    public double calculateSubtraction(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }

    public double calculateMultiplication(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    public double calculateDivision(double firstNumber, double secondNumber)  {
        if (secondNumber == 0) {
            throw new ArithmeticException("Division by zero");
        }
        return firstNumber / secondNumber;
    }
}
